<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Nico Heulsen
 * Date: 31/01/13
 * Time: 07:14
 */

function theme_webform_defaults_components_page($node, $selector_form, $components_form) {
  return $selector_form . $components_form;
}


function theme_webform_defaults_selector_form_table($form) {
  $output = '';

  $rows = array();
  $rows[] = array(
    'markup' => t('Select the webform of which you want to duplicate the fields:'),
    'webform' => drupal_render($form['webform']),
    'submit' => drupal_render($form['submit'])
  );

  $output .= theme('table', array(), $rows, array('id' => 'webform-default-selecor'));
  $output .= '<div class="hidden">'. drupal_render($form) .'</div>';

  return $output;
}
